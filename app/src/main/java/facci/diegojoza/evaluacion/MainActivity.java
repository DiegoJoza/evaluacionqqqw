package facci.diegojoza.evaluacion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    EditText edtnombre, edtapellido, edtCedula, edtdepartamento, edtfecha, edtoficina;
    Button btnAgregar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        edtnombre = findViewById(R.id.editText3);
        edtapellido = findViewById(R.id.editText4);
        edtCedula = findViewById(R.id.editText5);
        edtdepartamento = findViewById(R.id.editText8);
        edtoficina = findViewById(R.id.editText9);
        edtfecha = findViewById(R.id.editText10);
        btnAgregar = findViewById(R.id.button);


        final EVALUACIONDB evaluaciondb = new EVALUACIONDB(getApplicationContext());

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluaciondb.agregarDatos(edtnombre.getText().toString(),edtapellido.getText().toString(),edtCedula.getText().toString(),edtdepartamento.getText().toString(),edtfecha.getText().toString(),edtoficina.getText().toString());
                Toast.makeText(getApplicationContext(),"Se agrego correctamente",Toast.LENGTH_SHORT).show();
            }
        });







    }
}
