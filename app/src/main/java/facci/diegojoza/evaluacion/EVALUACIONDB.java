package facci.diegojoza.evaluacion;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class EVALUACIONDB extends SQLiteOpenHelper {


public static final String NOMBRE_DB="EVALUACION.DB";
public static final int VERSIOn_DB=1;
public static final String TABLA_DATOS = "CREATE TABLE DATOS(CEDULA TEXT PRIMARY KEY, NOMBRE TEXT, APELLIDO TEXT, DEPARTAMENTO TEXT, OFICINA TEXT, FECHA TEXT)";

    public EVALUACIONDB(@Nullable Context context) {
        super(context, NOMBRE_DB, null, VERSIOn_DB);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLA_DATOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL( "DROP TABLE IF EXISTS  TABLA_DATOS");
        db.execSQL(TABLA_DATOS);


    }

    public void agregarDatos(String NOMBRE, String APELLIDO, String CEDULA, String DEPARTAMENTO, String OFICINA, String FECHA){
        SQLiteDatabase DB= getWritableDatabase();
        if (DB!=null) {
            DB.execSQL("INSERT INTO  VALUES ('"+NOMBRE+" ',' "+APELLIDO+"','"+CEDULA+"','"+DEPARTAMENTO+"','"+OFICINA+"','"+FECHA+"')");
            DB.close();
        }

    }
}
